﻿namespace WOE_Launcher
{
    partial class firstRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFolder2 = new System.Windows.Forms.Button();
            this.txtFolder2 = new System.Windows.Forms.TextBox();
            this.btnConfirm2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // btnFolder2
            // 
            this.btnFolder2.Location = new System.Drawing.Point(244, 44);
            this.btnFolder2.Name = "btnFolder2";
            this.btnFolder2.Size = new System.Drawing.Size(28, 23);
            this.btnFolder2.TabIndex = 0;
            this.btnFolder2.Text = "...";
            this.btnFolder2.UseVisualStyleBackColor = true;
            // 
            // txtFolder2
            // 
            this.txtFolder2.Location = new System.Drawing.Point(13, 46);
            this.txtFolder2.Name = "txtFolder2";
            this.txtFolder2.Size = new System.Drawing.Size(225, 20);
            this.txtFolder2.TabIndex = 1;
            // 
            // btnConfirm2
            // 
            this.btnConfirm2.Location = new System.Drawing.Point(13, 73);
            this.btnConfirm2.Name = "btnConfirm2";
            this.btnConfirm2.Size = new System.Drawing.Size(259, 23);
            this.btnConfirm2.TabIndex = 2;
            this.btnConfirm2.Text = "Enregistrer";
            this.btnConfirm2.UseVisualStyleBackColor = true;
            // 
            // firstRun
            // 
            this.ClientSize = new System.Drawing.Size(284, 111);
            this.Controls.Add(this.btnConfirm2);
            this.Controls.Add(this.txtFolder2);
            this.Controls.Add(this.btnFolder2);
            this.Name = "firstRun";
            this.Load += new System.EventHandler(this.firstRun_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnFolder2;
        private System.Windows.Forms.TextBox txtFolder2;
        private System.Windows.Forms.Button btnConfirm2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;
    }
}