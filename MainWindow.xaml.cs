﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;

namespace WOE_Launcher
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Parameters param;

        WebClient webClient;               // Our WebClient that will be doing the downloading for us
        Stopwatch sw = new Stopwatch();
        public MainWindow()
        {
            InitializeComponent();
            param = new Parameters();

            if (File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg"))
            {
                using (var reader = new StreamReader(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg"))
                {
                    reader.ReadLine(); // skip
                    txtId.Text = reader.ReadLine();
                    if(txtId.Text != "")
                    {
                        cbReminder.IsChecked = true;
                    }
                }
            }
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            DBConnect con = new DBConnect();
            List <string> user = con.Select(txtId.Text);

            string password = Hash(txtId.Text.ToUpper() + ":" + txtPassword.Password.ToUpper());
            if (password == user[1].ToString())
            {
                var lastLogin = user[2].ToString();

                if (lastLogin == null || !File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg"))
                {
                    txtId.Visibility = System.Windows.Visibility.Hidden;
                    txtPassword.Visibility = System.Windows.Visibility.Hidden;
                    cbReminder.Visibility = Visibility.Hidden;
                    btnConnect.Visibility = System.Windows.Visibility.Hidden;
                    imgId.Visibility = System.Windows.Visibility.Hidden;
                    imgPassword.Visibility = System.Windows.Visibility.Hidden;
                    btnInstall.Visibility = Visibility.Visible;
                }
                else if (File.Exists(Directory.GetCurrentDirectory() + @"\config.cfg"))
                {
                    txtId.Visibility = System.Windows.Visibility.Hidden;
                    txtPassword.Visibility = System.Windows.Visibility.Hidden;
                    cbReminder.Visibility = Visibility.Hidden;
                    btnConnect.Visibility = System.Windows.Visibility.Hidden;
                    imgId.Visibility = System.Windows.Visibility.Hidden;
                    imgPassword.Visibility = System.Windows.Visibility.Hidden;
                    StreamReader readingFile = new StreamReader(Directory.GetCurrentDirectory() + @"\config.cfg");
                    string text = readingFile.ReadLine();
                    if(!File.Exists(text + @"\Wow-3.3.5.zip"))
                        btnInstall.Visibility = Visibility.Visible;
                    else
                    {
                        double length = new System.IO.FileInfo(text + @"\Wow-3.3.5.zip").Length;
                        Uri url = new Uri(@"https://worldofexodus.com/Wow-3.3.5.zip");
                        if (Math.Round(length / 1024.0 / 1024.0, 2) <= GetFileSize(url))
                            btnInstall.Visibility = Visibility.Visible;
                        else
                            btnJouer.Visibility = Visibility.Visible;
                    }
                    readingFile.Close();
                }
                else
                {
                    param.First = false;
                    if(File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg"))
                    {
                        txtId.Visibility = System.Windows.Visibility.Hidden;
                        txtPassword.Visibility = System.Windows.Visibility.Hidden;
                        cbReminder.Visibility = Visibility.Hidden;
                        btnConnect.Visibility = System.Windows.Visibility.Hidden;
                        imgId.Visibility = System.Windows.Visibility.Hidden;
                        imgPassword.Visibility = System.Windows.Visibility.Hidden;
                        btnJouer.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        FolderBrowserDialog folderDlg2 = new FolderBrowserDialog
                        {
                            ShowNewFolderButton = true
                        };
                        // Show the FolderBrowserDialog.  
                        bool isOK = showDirectory(folderDlg2);

                        if (isOK)
                        {
                            txtId.Visibility = System.Windows.Visibility.Hidden;
                            txtPassword.Visibility = System.Windows.Visibility.Hidden;
                            cbReminder.Visibility = Visibility.Hidden;
                            btnConnect.Visibility = System.Windows.Visibility.Hidden;
                            imgId.Visibility = System.Windows.Visibility.Hidden;
                            imgPassword.Visibility = System.Windows.Visibility.Hidden;
                            btnJouer.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            showDirectory(folderDlg2);
                        }
                    }
                }

                if (cbReminder.IsChecked == true)
                {
                    if (File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg"))
                    {
                        using (StreamReader sr = File.OpenText(Directory.GetCurrentDirectory() + @"\config.cfg"))
                        {
                            string[] lines = File.ReadAllLines(Directory.GetCurrentDirectory() + @"\config.cfg");
                            bool isMatch = false;
                            for (int x = 0; x < lines.Length - 1; x++)
                            {
                                if (txtId.Text == lines[x])
                                {
                                    sr.Close();
                                    isMatch = true;
                                }
                            }
                            if (!isMatch)
                            {
                                sr.Close();
                                File.AppendAllText(Directory.GetCurrentDirectory() + @"\config.cfg", "\n" + txtId.Text);
                            }
                        }
                    }
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Nom d'utilisateur ou mot de passe incorrect");
            }
        }

        static bool showDirectory(FolderBrowserDialog folderDlg2)
        {
            bool ret = false;
            DialogResult res = folderDlg2.ShowDialog();
            if (res.ToString() == "OK")
            {
                if (File.Exists(folderDlg2.SelectedPath + @"\Wow.exe"))
                {
                    File.WriteAllText(Directory.GetCurrentDirectory() + @"\config.cfg", folderDlg2.SelectedPath);
                    ret = true;
                }
                else
                {
                    System.Windows.MessageBox.Show("Répertoire invalide ! Le répertoire cible doit contenir Wow.exe");
                    ret = false;
                }
            }
            return ret;
        }

        private static double GetFileSize(Uri uriPath)
        {
            var webRequest = HttpWebRequest.Create(uriPath);
            webRequest.Method = "HEAD";

            using (var webResponse = webRequest.GetResponse())
            {
                var fileSize = webResponse.Headers.Get("Content-Length");
                var fileSizeInMegaByte = Math.Round(Convert.ToDouble(fileSize) / 1024.0 / 1024.0, 2);
                return fileSizeInMegaByte;
            }
        }

        static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }

        private void btnInstall_Click(object sender, RoutedEventArgs e)
        {
            string path = @"C:\Program Files";
            firstInstall Form1 = new firstInstall(path, param);
            Form1.ShowDialog();

            if (param.FullPath.Length > 0 && param.first)
            {
                btnInstall.Visibility = System.Windows.Visibility.Hidden;
                progressBar.Visibility = System.Windows.Visibility.Visible;
                labelSpeed.Visibility = System.Windows.Visibility.Visible;
                labelPerc.Visibility = System.Windows.Visibility.Visible;
                labelDownloaded.Visibility = System.Windows.Visibility.Visible;

                //DownloadFile(@"https://worldofexodus.com/Wow-3.3.5.zip", param.FullPath);
                labelDownloaded.Content = "En cours...";
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);

                StreamReader readingFile = new StreamReader(Directory.GetCurrentDirectory() + @"\config.cfg");
                string text = readingFile.ReadLine();

                if (Directory.Exists(param.FullPath))
                    client.DownloadFileAsync(new Uri(@"https://worldofexodus.com/Wow-3.3.5.zip"), text + @"\Wow-3.3.5.zip");
                else
                    System.Windows.MessageBox.Show("Répertoire incorrect !");
            }
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());

            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;

            progressBar.Value = int.Parse(Math.Truncate(percentage).ToString());

            // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
            labelSpeed.Content = string.Format("{0} MB's / {1} GB's",
                (e.BytesReceived / 1024d / 1024d).ToString("0.00"),
                (e.TotalBytesToReceive / 1024d / 1024d / 1024d).ToString("0.00"));
            // Show the percentage on our label.
            labelPerc.Content = e.ProgressPercentage.ToString() + "%";
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            labelDownloaded.Content = "Terminé !";
            string pathDowload = param.FullPath;
            if (!pathDowload.EndsWith("\\"))
                pathDowload = pathDowload + "\\";

            File.WriteAllText(Directory.GetCurrentDirectory() + @"\config.cfg", pathDowload);
            ZipFile.ExtractToDirectory(Directory.GetCurrentDirectory() + @"\Wow-3.3.5.zip", pathDowload + @"\Wow-3.3.5");
            File.Delete(Directory.GetCurrentDirectory() + @"\Wow-3.3.5.zip");

            progressBar.Visibility = Visibility.Hidden;
            labelSpeed.Visibility = Visibility.Hidden;
            labelPerc.Visibility = Visibility.Hidden;
            labelDownloaded.Visibility = Visibility.Hidden;

            btnJouer.Visibility = Visibility.Visible;
        }



        public void DownloadFile(string urlAddress, string location)
        {
            using (webClient = new WebClient())
            {
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

                // The variable that will be holding the url address (making sure it starts with http://)
                Uri URL = urlAddress.StartsWith("https://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri(urlAddress);
                // Start the stopwatch which we will be using to calculate the download speed
                sw.Start();

                try
                {
                    // Start downloading the file
                    webClient.DownloadFileAsync(URL, location);
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }

        // The event that will fire whenever the progress of the WebClient is changed
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // Calculate download speed and output it to labelSpeed.
            labelSpeed.Content = string.Format("{0} kb/s", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));

            // Update the progressbar percentage only when the value is not the same.
            progressBar.Value = e.ProgressPercentage;

            // Show the percentage on our label.
            labelPerc.Content = e.ProgressPercentage.ToString() + "%";

            // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
            labelDownloaded.Content = string.Format("{0} MB's / {1} MB's",
                (e.BytesReceived / 1024d / 1024d).ToString("0.00"),
                (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
        }

        // The event that will trigger when the WebClient is completed
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            // Reset the stopwatch.
            sw.Reset();

            if (e.Cancelled == true)
            {
                System.Windows.MessageBox.Show("Download has been canceled.");
            }
            else
            {
                System.Windows.MessageBox.Show("Download completed!");
            }
        }

        private void btnJouer_click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg"))
            {
                StreamReader readingFile = new StreamReader(Directory.GetCurrentDirectory() + @"\config.cfg");
                string text = readingFile.ReadLine();
                Process.Start(text + @"\Wow.exe");
                readingFile.Close();
            }
            Close();
        }
    }
}
