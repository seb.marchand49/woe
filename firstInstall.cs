﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace WOE_Launcher
{
    public partial class firstInstall : Form
    {
        public Parameters param1;
        public firstInstall(string path, Parameters param)
        {
            InitializeComponent();
            if (File.Exists(Directory.GetCurrentDirectory() + @"\config.cfg"))
            {
                StreamReader readingFile = new StreamReader(Directory.GetCurrentDirectory() + @"\config.cfg");
                string text = readingFile.ReadLine();
                txtFolder.Text = text;
                readingFile.Close();
            }
            else
                txtFolder.Text = path;
            param1 = param;
        }

        private void firstInstall_Load(object sender, EventArgs e)
        {

        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true
            };
            // Show the FolderBrowserDialog.  
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFolder.Text = folderDlg.SelectedPath;
                _ = folderDlg.RootFolder;
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllText(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg", txtFolder.Text);
            param1.FullPath = txtFolder.Text;
            Close();
        }
    }
}
