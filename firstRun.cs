﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WOE_Launcher
{
    public partial class firstRun : Form
    {
        public Parameters param1;
        public firstRun(string path, Parameters param)
        {
            InitializeComponent();
            txtFolder2.Text = path;
            param1 = param;
        }

        private void firstRun_Load(object sender, EventArgs e)
        {

        }

        private void btnFolder2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg2 = new FolderBrowserDialog
            {
                ShowNewFolderButton = true
            };
            // Show the FolderBrowserDialog.  
            DialogResult result2 = folderDlg2.ShowDialog();
            if (result2 == DialogResult.OK)
            {
                txtFolder2.Text = folderDlg2.SelectedPath;
                _ = folderDlg2.RootFolder;
            }
        }

        private void btnConfirm2_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllText(System.IO.Directory.GetCurrentDirectory() + @"\config.cfg", txtFolder.Text);
            param1.FullPath = txtFolder2.Text;
            Close();
        }
    }
}
