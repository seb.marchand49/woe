﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOE_Launcher
{
    public class Parameters
    {
        public string fullPath;
        public bool first = true;

        public Parameters()
        {
            Initialize();
        }

        private void Initialize()
        {
        }

        public string FullPath { get; set; }
        public bool First { get; set; }
    }
}
